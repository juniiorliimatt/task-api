package com.tasks.tasks;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@RestController
@RequestMapping("/tasks")
public class TaskController{

  private final TaskService service;

  @GetMapping("/")
  public ResponseEntity<List<TaskDto>> findAll(){
	return ResponseEntity.status(OK)
						 .body(service.findAll()
									  .stream()
									  .map(TaskDto::convertToDto)
									  .collect(Collectors.toList()));
  }

  @GetMapping("/{id}")
  public ResponseEntity<TaskDto> findById(@PathVariable("id") UUID id){
	Optional<TaskModel> task = service.findById(id);
	if(task.isEmpty()){
	  return ResponseEntity.status(NOT_FOUND)
						   .build();
	}

	TaskDto dto = TaskDto.convertToDto(task.get());
	return ResponseEntity.status(OK)
						 .body(dto);
  }

  @PostMapping
  public ResponseEntity<TaskDto> save(@Valid @RequestBody TaskDto dto){
	dto.setCreatedAt(LocalDateTime.now());
	dto.setUpdatedAt(LocalDateTime.now());
	return ResponseEntity.status(CREATED)
						 .body(TaskDto.convertToDto(service.save(TaskDto.convertToModel(dto))));
  }

  @PatchMapping("/{id}")
  public ResponseEntity<TaskDto> isFinished(@Valid @PathVariable("id") UUID id){
	Optional<TaskModel> taskFromBD = service.findById(id);
	if(taskFromBD.isEmpty()){
	  return ResponseEntity.status(NOT_FOUND)
						   .build();
	}

	TaskModel task = taskFromBD.get();
	task.setUpdatedAt(LocalDateTime.now());
	task.setFinished(true);
	service.save(task);
	return ResponseEntity.status(OK).build();
  }

  @PutMapping("/{id}")
  public ResponseEntity<TaskDto> update(@Valid @PathVariable("id") UUID id, @RequestBody TaskDto dto){
	Optional<TaskModel> task = service.findById(id);
	if(task.isEmpty()){
	  return ResponseEntity.status(NOT_FOUND)
						   .build();
	}
	dto.setUpdatedAt(LocalDateTime.now());
	return ResponseEntity.status(OK)
						 .body(TaskDto.convertToDto(service.update(TaskDto.convertToModel(dto))));
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Object> delete(@PathVariable("id") UUID id){
	Optional<TaskModel> task = service.findById(id);
	if(task.isEmpty()){
	  return ResponseEntity.status(NOT_FOUND)
						   .build();
	}
	service.deleteById(id);
	return ResponseEntity.status(OK)
						 .build();
  }

}
