package com.tasks.tasks;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto implements Serializable{

  private UUID id;

  @NotEmpty
  private String tittle;

  @NotEmpty
  private String description;

  @NotNull
  private Boolean finished = false;

  private LocalDateTime createdAt;
  private LocalDateTime updatedAt = LocalDateTime.now();

  public static TaskModel convertToModel(TaskDto dto){
	ModelMapper mapper = new ModelMapper();
	return mapper.map(dto, TaskModel.class);
  }

  public static TaskDto convertToDto(TaskModel model){
	ModelMapper mapper = new ModelMapper();
	return mapper.map(model, TaskDto.class);
  }

}
