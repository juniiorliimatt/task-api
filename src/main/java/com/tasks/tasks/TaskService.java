package com.tasks.tasks;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TaskService{

  List<TaskModel> findAll();

  Optional<TaskModel> findById(UUID id);

  TaskModel save(TaskModel taskModel);

  TaskModel update(TaskModel taskModel);

  void deleteById(UUID id);

}
