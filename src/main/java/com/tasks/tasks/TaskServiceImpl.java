package com.tasks.tasks;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService{

  private final TaskRepository repository;


  @Override
  public List<TaskModel> findAll(){
	return repository.findAll();
  }

  @Override
  public Optional<TaskModel> findById(UUID id){
	return repository.findById(id);
  }

  @Override
  public TaskModel save(TaskModel taskModel){
	return repository.save(taskModel);
  }

  @Override
  public TaskModel update(TaskModel taskModel){
	return repository.save(taskModel);
  }

  @Override
  public void deleteById(UUID id){
	repository.deleteById(id);
  }

}
